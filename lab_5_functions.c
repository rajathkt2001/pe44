#include<stdio.h>
void read_array(int a[],int n)
{
	int i;
	printf("enter the values of your array\n");
	for(i=0;i<n;i++)
		scanf("%d",&a[i]);
}
int find_largest(int a[],int n)
{
	int i,large,large_pos;	
	large=a[0];
	large_pos=0;
	for(i=1;i<n;i++)
	{
		if(a[i]<large)
		{
			large=a[i];
			large_pos=i;
		}
	}
	printf("the largest number =%d at position=%d\n",large,large_pos);
	return large_pos;
}
int find_smallest(int a[],int n)
{
	int i,small,small_pos;
	small=a[0];
	small_pos=0;
	for(i=1;i<n;i++)
	{
		if(a[i]>small)
		{
			small = a[i];
			small_pos=i;
		}
	}
	printf("the smallest number =%d at position=%d\n",small,small_pos); 
	return small_pos;
}
void interchange(int a[],int large_pos,int small_pos,int temp)
{
	temp=a[large_pos];
	a[large_pos]=a[small_pos];
	a[small_pos]=temp;
}
void print_array(int a[],int n)
{
	int i;
	printf("the array after interchanging smallest and largest positions are\n");
	for(i=0;i<n;i++)
		printf("%d\t",a[i]);
}
int main()
{
	int a[20],n,large,small,large_pos,small_pos,temp;
	printf("enter the size of your array\n");
	scanf("%d",&n);
	read_array(a,n);
	large_pos=find_largest(a,n);
	small_pos=find_smallest(a,n);
	interchange(a,large_pos,small_pos,temp);
	print_array(a,n);
	return 0;
}	