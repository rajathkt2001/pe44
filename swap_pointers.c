#include<stdio.h>
void swap(int *a,int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}	
int main()
{
	int *a,*b;
	printf("enter two numbers to be swapped\n");
	printf("enter a:\n");
	scanf("%d",&a);
	printf("enter b:\n");
	scanf("%d",&b);
	swap(&a,&b);
	printf("after swapping a:%d b:%d\n",a,b);
	return 0;
}	
		