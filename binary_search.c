#include<stdio.h>

int main()
{
	int a[20],n,i,beg,key,end,mid;
	printf("enter your array size\n");
	scanf("%d",&n);
	printf("enter array\n");
	for(i=0;i<n;i++)
		scanf("%d",&a[i]);
	printf("enter the key to be searched\n");
	scanf("%d",&key);
	beg=0;
	end=n-1;		
	while(beg<=end)
	{
		mid=(beg+end)/2;
		if (a[mid]==key)
		{
			printf("your element %d is found at pos %d\n",key,mid);
			break;
		}
		else if(a[mid]>key)
		{
			end=mid-1;
		}
		else
			beg=mid+1;
	}
	if(beg>end)
	{
		printf("your element is not found\n");
	}
	return 0;
}	