#include<stdio.h>
#include<math.h>
#define pi 3.14
float input()
{
	float r;
	printf("enter radius");
	scanf("%f", &r);
	return r;
}

float compute_area(float r)
{
	float a;
	a = pi * r * r;
	return a;
}

float compute_circumference(float r)
{
	float c;
	c = 2 * pi * r;
	return c;
}

float output_area(float r, float a)
{
	printf("the area of the circle is=%f", a);
}

float output_circumference(float r, float c)
{
	printf("the circumference of the circle is=%f", c);
}

int main()
{
	float r, a, c;
	r = input();
	a = compute_area(r);
	c = compute_circumference(r);
	output_area(r, a);
	output_circumference(r, c);
	return 0;
}